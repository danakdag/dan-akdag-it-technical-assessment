# Using numpy and pandas

import pandas as pd
import numpy as np


filename = './GitLabDepartmentGroups.xlsx'
fileout = open('ArrayOut.txt', 'w')

# Read excel sheet using pandas
df = pd.read_excel(io=filename)

# Dump entire dataframe to a whole_array using numpy
whole_data = df.to_numpy()

# Define number of rows and number of columns
num_rows, num_cols = whole_data.shape

# Define interested column given for gl_dept_group
column_interest = 3 

# Define suffixes required to remove
suff_1= '-shared-infra'
suff_2= '-shared-services'

# Define to single row array
drop_1  = [0 for i in range(num_rows)]
drop_2  = [0 for i in range(num_rows)]

#Copy column 3 into an array
for i in range(num_rows):
    drop_1[i] = str(whole_data[i][column_interest])

#Drop '-shared-infra' from column 3 rows
for i in range(num_rows):
    if drop_1[i].endswith(suff_1):
        drop_1[i] = drop_1[i].replace(suff_1, '')
  
#Drop '-shared-services' rows from column
for i in range(num_rows):
    if drop_1[i].endswith(suff_2):
        drop_2[i] = ''
    else:
        drop_2[i] = drop_1[i]

#Print data from array
for i in range(num_rows):
    if drop_2[i] != '':
        print(str(drop_2[i]))
        


