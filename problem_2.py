import yaml

with open(r'stages.yml') as file:
    stages_dict = yaml.full_load(file)

def nested_dict_values_iterator(dict_obj):
   
    # Iterate over all values of given dictionary
    for value in dict_obj.values():
        # Check if value is of dict type
        if isinstance(value, dict):
            # If value is dict then iterate over all its values
            for v in  nested_dict_values_iterator(value):
                yield v
        else:
            # If value is not dict type then yield the value
            yield value

#Define the special keys using a tuple
stages_keys =  ('pm' ,
                'pmm', 
                'cm', 
                'backend_engineering_manager', 
                'frontend_engineering_manager', 
                'support',
                'sets', 
                'pdm', 
                'ux', 
                'uxr', 
                'tech_writer', 
                'tw_backup', 
                'appsec_engineer') 

length_stages_keys = len(stages_keys)
# print(length_stages_keys)

for x in range(length_stages_keys):
    print(stages_keys[x])

# print(stages_dict)

for x in range(length_stages_keys):
    for value in nested_dict_values_iterator(stages_dict):
        if value == str(stages_keys[x]):
            print(stages_keys[x])
            print(value)

#This code is incomplete. I am including the current code that I have written so far for Problem 2. The yaml file contains a dictionary with multiple sub-indexes. I am working on a solution to understand how it relates to arrays in python and how to sort a relational database and print values pertaining to names. But my current knowledge lacks the understanding of this. I understand the problem and how to get there, but just do not understand how to write the code properly. 


